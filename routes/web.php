<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('events/add', 'EventsController@create')->name('events.create');
Route::post('events/add', 'EventsController@store')->name('events.store');
Route::get('notify/add', 'NotifyController@create')->name('notify.create');
Route::post('notify/add', 'NotifyController@store')->name('notify.store');
Route::get('notify/detail/{id}', 'NotifyController@show')->name('notify.show');
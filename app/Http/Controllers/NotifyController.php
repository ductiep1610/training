<?php

namespace App\Http\Controllers;

use Auth;
use App\Notify;
use App\UserNotification;
use Illuminate\Http\Request;

class NotifyController extends Controller
{
    public function create()
    {
        return view('admin.notify.add');
    }

    public function store(Request $request)
    {
       
        $data = [
            'title' =>$request->title,
            'content' =>$request->content,
            'user_id' => Auth::id()
        ];
        Notify::create($data);
        return redirect()->back();
    }

    public function show($id)
    {
        $notification = Notify::find($id);
        $user_id = auth()->id();
        $exists = UserNotification::where([
            'user_id' => $user_id,
            'notify_id' => $notification->id
        ])->first('id');
        
        if(!$exists) {
            UserNotification::create([
                'user_id' => $user_id,
                'notify_id' => $notification->id,
            ]);
        }
        
        return view('admin.notify.detail');
    }
}

<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $events = Events::orderBy('created_at', 'DESC')->paginate(2);
         if($request->ajax()) {
            return response()->json([
                'events' => $events
            ], 200);
        }

        $doc = new DocumentDirector();
        $doc->setTitle('title')->setLogo('logo')->setContent('content');
        $contract = $doc->buildDocument(new Contract());
        dd($contract->sign());
        return view('admin.dashboard', compact('events'));
    }
}

class DocumentDirector
{
    protected $title;
    protected $logo;
    protected $template;
    protected $content;

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function buildDocument(DocumentBuilder $document)
    {
        if ($this->title) {
            $document->setTitle($this->title);
        }

        if ($this->logo) {
            $document->setLogo($this->logo);
        }

        if ($this->template) {
            $document->setTemplate($this->template);
        }

        if ($this->content) {
            $document->setContent($this->content);
        }

        return $document;
    }
}

interface DocumentBuilder
{
    public function setTitle($title);
    public function setLogo($logo);
    public function setTemplate($template);
    public function setContent($content);
}

interface SignAble
{
    public function sign();
}

abstract class Document
{
    public $title;
    public $template;
    public $content;
    public $logo;
} 

class Contract extends Document implements DocumentBuilder, SignAble
{
    public function setTitle($title)
    {
        $this->title = '[CONTRACT]' . $title;
    }

    public function setLogo($logo)
    {
        $this->logo = '[CONTRACT]' . $logo;
    }

    public function setTemplate($template)
    {
        $this->template = '[CONTRACT]' . $template;
    }

    public function setContent($content)
    {
        $this->content = '[CONTRACT]' . $content;
    }

    public function sign()
    {
        echo "[Contract] sign";
    }
}

class Note extends Document implements DocumentBuilder
{
    public function setTitle($title)
    {
        $this->title = '[NOTE]' . $title;
    }

    public function setLogo($logo)
    {
        $this->logo = '[NOTE]' . $logo;
    }

    public function setTemplate($template)
    {
        $this->template = '[NOTE]' . $template;
    }

    public function setContent($content)
    {
        $this->content = '[NOTE]' . $content;
    }
}
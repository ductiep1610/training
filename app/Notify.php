<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    protected $table = 'notifies';
    protected $fillable = ['title', 'content', 'user_id'];

    // public function readByUsers()
    // {
    //     return $this->belongsToMany('App\User', 'user_notifications', 'notify_id', 'user_id');
    // }
}

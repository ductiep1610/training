<?php 

use App\Notify;
use Illuminate\Support\Facades\DB;

if (!function_exists('getUnreadNotificationByUser')) {
    function getUnreadNotificationByUser($userId) {
        $total = Notify::select('id')->where('user_id', '<>', $userId)->get()->count();
        $readed = DB::table('user_notifications')
            ->where('user_id', $userId)
            ->select('id')
            ->get()
            ->count();

        return $total - $readed;
    }
}

if (!function_exists('unReadNotificationCollectionByUser')) {
    function unReadNotificationCollectionByUser($userId) {
        $readedIds = DB::table('notifies')
            ->join('user_notifications', function($query) {
                $query->on('notifies.id', '=', 'user_notifications.notify_id');
            })
            ->where('user_notifications.user_id', $userId)
            ->pluck('notifies.id')
            ->toArray();

        return Notify::where('user_id', '<>', $userId)
            ->whereNotIn('id', $readedIds)
            ->get();
    }
}

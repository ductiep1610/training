<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-wrap">
                <form class="form-header" action="" method="POST">
                    <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                    <button class="au-btn--submit" type="submit">
                        <i class="zmdi zmdi-search"></i>
                    </button>
                </form>
                {{-- @php
                    $notifycations = \App\Notify::latest()->get();
                    $total = $notifycations->count();
                    $readedNotification = App\Notify::whereHas('readByUsers', function($q) {
                        $q->where('user_id', auth()->id());
                    })->get();
                    $notReadNotifications = App\Notify::whereHas('readByUsers', function($q) {
                        $q->where('user_id', '!=', auth()->id());
                    })->get();
                    $notReadNotificationsNumber = $total - $readedNotification->count();
                @endphp --}}
                <div class="header-button">
                    {{-- @if ($notReadNotificationsNumber) --}}
                    <div class="noti-wrap">
                        <div class="noti__item js-item-menu">
                            <i class="zmdi zmdi-notifications"></i>
                            @if (getUnreadNotificationByUser(auth()->id()))
                                <span class="quantity">{{ getUnreadNotificationByUser(auth()->id()) }}</span>
                            @endif
                            <div class="notifi-dropdown js-dropdown">
                                <div class="notifi__title">
                                    <p>You have {{ getUnreadNotificationByUser(auth()->id()) }} Notifications</p>
                                </div>
                                @if (getUnreadNotificationByUser(auth()->id()) > 0)
                                    @foreach (unreadNotificationCollectionByUser(auth()->id()) as $notify)
                                        <div class="notifi__item">
                                            <div class="bg-c1 img-cir img-40">
                                                <i class="zmdi zmdi-email-open"></i>
                                            </div>
                                            <a href="{{ route('notify.show', $notify->id) }}">
                                                <div class="content">
                                                    <p>{{ $notify->title }}</p>
                                                    <span class="date">{{ Carbon\Carbon::parse($notify->created_at)->diffForHumans() }}</span>
                                                </div>
                                            </a> 
                                        </div>
                                    @endforeach
                                @else
                                <div class="notifi__item">
                                        <div class="content">
                                            <p>Không có thông báo mới nào!</p>
                                        </div>
                                    </a> 
                                </div>
                                @endif
                                
                                <div class="notifi__footer">
                                    <a href="#">All notifications</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @endif --}}
                    <div class="account-wrap">
                        <div class="account-item clearfix js-item-menu">
                            <div class="image">
                                <img src="{{ asset('admin/images/icon/avatar-01.jpg') }}" alt="John Doe" />
                            </div>
                            <div class="content">
                                <a class="js-acc-btn" href="#">john doe</a>
                            </div>
                            <div class="account-dropdown js-dropdown">
                                <div class="info clearfix">
                                    <div class="image">
                                        <a href="#">
                                            <img src="{{ asset('admin/images/icon/avatar-01.jpg') }}" alt="John Doe" />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h5 class="name">
                                            <a href="#">john doe</a>
                                        </h5>
                                        <span class="email">johndoe@example.com</span>
                                    </div>
                                </div>
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i>Account</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-settings"></i>Setting</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-money-box"></i>Billing</a>
                                    </div>
                                </div>
                                <div class="account-dropdown__footer">
                                    <a href="{{ route('logout') }}">
                                        <i class="zmdi zmdi-power"></i>Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
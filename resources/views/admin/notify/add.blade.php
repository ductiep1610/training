@extends('home')

@section('content')
<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
            <strong>Notify</strong> Add
        </div>
        <div class="card-body card-block">
            <form action="{{ route('notify.store') }}" method="post" class="form-horizontal">
                @csrf
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="hf-email" class=" form-control-label">Title</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text"  name="title"  class="form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="hf-password" class=" form-control-label">Content</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text"  name="content" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
            </form>
        </div>
    </div>
@endsection